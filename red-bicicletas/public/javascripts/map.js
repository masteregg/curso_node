var map = L.map('main_map').setView([-33.5401462,-70.635563,14], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  
}).addTo(map);

$.ajax({
    dataType:"json",
    url:"api/bicicletas",
    success:(result)=>{
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion,{title:bici.id}).addTo(map);
        });
    }
});

