let Bicicleta = function(id,color,modelo,ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.allBicis = [];
Bicicleta.add = (aBicis)=>{
    Bicicleta.allBicis.push(aBicis);
}

Bicicleta.findById =(aBiciId)=>{
    let bici = Bicicleta.allBicis.find(x=>x.id ==aBiciId);
    if(bici)
        return bici
    else
    throw new Error(`No existe bicicleta con el id ${aBiciId}`);
    
}

Bicicleta.removeById =(aBiciId)=>{
    for(let i = 0;i<Bicicleta.allBicis.length;i++){
        if(Bicicleta.allBicis[i].id==aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}

let a = new Bicicleta(1,'blanca','urbana',[-33.538396, -70.636164]);
let b = new Bicicleta(2,'roja','urbana',[-33.539376, -70.632302]);

Bicicleta.add(a);
Bicicleta.add(b);
module.exports = Bicicleta;